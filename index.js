const express = require('express');
const cors = require('cors')

const app = express();
const PORT = 4000;

//Middleswares-->Permite el intercambio de info entre back y front
app.use(express.json())
app.use(cors())


app.listen(PORT, ()=>{
    console.log(`SERV LISTENING PORT ${PORT}`)
})

module.exports=app